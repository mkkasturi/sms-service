package com.wavelabs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.wavelabs.service.KafkaConsumer;
@Configuration
@SpringBootApplication
@ComponentScan({
		"com.wavelabs.dao, com.wavelabs, com.wavelabs.service, com.wavelabs.resource" })
@Import(KafkaConsumer.class)
public class SmsServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmsServiceApplication.class, args);
		KafkaConsumer consumer = new KafkaConsumer();
		consumer.start();
	}
}
