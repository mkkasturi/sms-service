package com.wavelabs.service;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;

import org.springframework.stereotype.Component;

import kafka.consumer.Consumer;
import kafka.consumer.ConsumerConfig;
import kafka.consumer.ConsumerIterator;
import kafka.consumer.KafkaStream;
import kafka.javaapi.consumer.ConsumerConnector;
import kafka.javaapi.message.ByteBufferMessageSet;
import kafka.message.MessageAndOffset;

@Component
public class KafkaConsumer extends Thread {
	
	TwilioService twilio = new TwilioService();
	
	
	private final static Logger LOGGER = Logger.getLogger(KafkaConsumer.class.getName());
	
	final static String clientId = "SimpleConsumerDemoClient";
	final static String TOPIC = "topicK";
	ConsumerConnector consumerConnector;
	

	public static void main(String[] argv) throws UnsupportedEncodingException {
		KafkaConsumer kafkaConsumer = new KafkaConsumer();
		kafkaConsumer.start();
	}

	public KafkaConsumer() {
		Properties properties = new Properties();
		properties.put("zookeeper.connect", "10.9.9.43:2181");
		properties.put("group.id", "test-group");
		ConsumerConfig consumerConfig = new ConsumerConfig(properties);
		consumerConnector = Consumer.createJavaConsumerConnector(consumerConfig);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void run() {
		Map<String, Integer> topicCountMap = new HashMap<String, Integer>();
		topicCountMap.put(TOPIC, new Integer(1));
		Map<String, List<KafkaStream<byte[], byte[]>>> consumerMap = consumerConnector
				.createMessageStreams(topicCountMap);
		List<KafkaStream<byte[], byte[]>> stream = consumerMap.get(TOPIC);
		for(KafkaStream<?, ?> st : stream) {
			ConsumerIterator<byte[], byte[]> it = (ConsumerIterator<byte[], byte[]>) st.iterator();
			while (it.hasNext()) {
				String data = new String(it.next().message());
				String[] strings = data.split("-", 2);
				String phonenNumber = strings[0];
				String message = strings[1];
				LOGGER.info(message);
				twilio.sendSMS("+91"+phonenNumber, message);
			}
		}
	}

	@SuppressWarnings("unused")
	private static void printMessages(ByteBufferMessageSet messageSet) throws UnsupportedEncodingException {
		for (MessageAndOffset messageAndOffset : messageSet) {
			ByteBuffer payload = messageAndOffset.message().payload();
			byte[] bytes = new byte[payload.limit()];
			payload.get(bytes);
			LOGGER.info(new String(bytes, "UTF-8"));
		}
	}
}