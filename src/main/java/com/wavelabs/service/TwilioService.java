package com.wavelabs.service;

import org.springframework.stereotype.Component;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

@Component
public class TwilioService {
	public void sendSMS(String phonenumber, String message) {
		final String ACCOUNT_SID = "ACadd3a571fa09ef3cfa924698d162719f";
		final String AUTH_TOKEN = "076d4c0effcad559d47af4189ad37ad5";
		Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
		Message.creator(new PhoneNumber(phonenumber), new PhoneNumber("+12052361808"), message).create();
	}
}
