package com.wavelabs.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavelabs.service.TwilioService;

@RestController
@Component
public class MessageResource {
	@Autowired
	TwilioService twilio;

	@RequestMapping(value = "/twilio", method = RequestMethod.POST)
	public void sendMessage(@RequestParam("phone") String phoneNumber, @RequestParam("message") String message) {
		twilio.sendSMS(phoneNumber, message);
	}
}
