SMS Service

This is a microservice which sends an SMS to the mobile using Twilio API. When it gets a message from Publisher it sends that message to the member.

Prerequisites:
To run this application your machine Zookeeper and Kafka both has to be installed on your machine.

Note:  In the project KafkaConsumer location is src/main/java/com/wavelabs/service/KafkaConsumer.java


