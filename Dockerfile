FROM java:8
ADD target/smsservice.jar smsservice.jar
ENTRYPOINT ["java","-jar","smsservice.jar"]
